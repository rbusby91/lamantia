<!--  Carousel - consult the Twitter Bootstrap docs at
      http://twitter.github.com/bootstrap/javascript.html#carousel -->
<div id="slido" class="carousel slide carousel-fade"><!-- class of slide for animation -->
  <div class="carousel-inner">
  
	<?php if (have_posts()) : $count=1; ?>
    <?php while ( $loop->have_posts() ) : $loop->the_post(); ?>
    <div class="<?php if($count=="1"){ echo 'active' ; } ?> item"><!-- class of active since it's the first item -->
    		<a href="<?php the_field('slider_link'); ?>">
    		<?php the_post_thumbnail('slider');?>
        	<div class="slider-title"><?php the_title(); ?></div>
            </a>
      </div>
    <?php $count++; endwhile;?>
    <?php endif; ?>
    		
  </div><!-- /.carousel-inner -->
 
  <!--  Next and Previous controls below
        href values must reference the id for this carousel -->
    <a class="carousel-control left" href="#slido" data-slide="prev">&lsaquo;</a>
    <a class="carousel-control right" href="#slido" data-slide="next">&rsaquo;</a>
</div><!-- /.carousel -->