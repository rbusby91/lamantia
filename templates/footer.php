<footer class="content-info" role="contentinfo">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="row">
					<ul class="list-unstyled">
						<li class="col-sm-4"><strong>Lamantia</strong></li>
						<li class="col-sm-4"><strong>Sectors</strong></li>
						<li class="col-sm-4"><strong>Social Media</strong></li>
					</ul>
				</div>
				<div class="row">
					<ul class="list-unstyled col-sm-4">
						<?php dynamic_sidebar('footer-menu'); ?>
						<li><a href="#">Careers</a></li>
					</ul>
					<ul class="list-unstyled col-sm-4">
						<li><a href="#">Residential</a></li>
						<li><a href="#">Commercial</a></li>
					</ul>
					<ul class="list-unstyled col-sm-4">
						<li><a target="_blank" href="http://www.facebook.com/lamantiaconstruction?ref=ts&fref=ts">Facebook</a></li>
						<li><a target="_blank" href="https://twitter.com/lamantiadesign">Twitter</a></li>
						<li><a target="_blank" href="http://pinterest.com/lamantiadesign/">Pinterest</a></li>
						<li><a target="_blank" href="#http://www.houzz.com/pro/lamantia/lamantia-design-construction">Houzz</a></li>
					</ul>
				</div>
			</div>
		</div>
		<div class="row">
			<ul class="footer-logos col-sm-12 list-inline list-unstyled tc clearfix">
				<li><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/EPA.png" alt=""></li>
				<li><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/NARI.png" alt=""></li>
				<li><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/nkba.png" alt=""></li>
				<li><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/Pella.png" alt=""></li>
			</ul>
		</div>
		<div class="row">
			<div class="col-lg-12 tr">
				<p>&copy; <?php echo date('Y'); ?> <?php bloginfo('name'); ?></p>
			</div>
		</div>
	</div>
</footer>

<script>
    jQuery( '.carousel-inner').find('.item:first' ).addClass( 'active' );
</script>

<?php wp_footer(); ?>
