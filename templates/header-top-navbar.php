<div class="section-top-container clearfix">
  <div class="container">
    <div class="row">
      <div class="col-lg-12">
        <ul class="list-inline list-unstyled pull-left">
          <li><a target="_blank" href="http://www.facebook.com/lamantiaconstruction?ref=ts&fref=ts"><i class="fa fa-facebook"></i></a></li>
          <li><a target="_blank" href="https://twitter.com/lamantiadesign"><i class="fa fa-twitter"></i></a></li>
          <li><a target="_blank" href="http://pinterest.com/lamantiadesign/"><i class="fa fa-pinterest"></i></a></li>
          <li><a target="_blank" href="http://www.houzz.com/pro/lamantia/lamantia-design-construction"><i class="fa fa-houzz"></i></a></li>
        </ul>
        <ul class="list-inline list-unstyled pull-right">
          <li><i class="fa fa-map-marker"></i> 9100 Ogden Avenue Brookfield, IL</li>
          <li><i class="fa fa-phone"></i> <strong>708.387.9900</strong></li>
        </ul>
      </div>
    </div>
  </div>
</div>
<header class="banner navbar navbar-default navbar-static-top" role="banner">
  <div class="container">
    <div class="row">
      <div class="col-lg-12">
        <a class="navbar-brand" href="<?php echo home_url(); ?>/">
          <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/logo.png" alt="<?php bloginfo('name'); ?>">
          <!-- <p class="tc">Adding value since 1973</p> -->
        </a>
      </div>
    </div>
  </div>
  <div class="navbar-container">
    <div class="container">
      <div class="row">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
        </div>
        <nav class="collapse navbar-collapse" role="navigation">
          <?php
            if (has_nav_menu('primary_navigation')) :
              wp_nav_menu(array('theme_location' => 'primary_navigation', 'menu_class' => 'nav navbar-nav'));
            endif;
          ?>
        </nav>
      </div>
    </div>
  </div>
</header>
