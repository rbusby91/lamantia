<?php while (have_posts()) : the_post(); ?>
<article <?php post_class(); ?>>
	<header>
		<h2 class="entry-title"><?php the_title(); ?></h2>
	</header>
	<div class="entry-summary">
		<div class="post-thumbnail">
		<?php if ( has_post_thumbnail() )
		{ the_post_thumbnail(); }
		?>
		</div>
		<?php the_content(); ?>
	</div>
	<footer class="meta">
		<?php get_template_part('templates/entry-meta'); ?>
	</footer>
</article>
<?php endwhile; ?>
