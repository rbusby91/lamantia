<?php
/**
 * Custom functions
 */
function site() {
   echo site_url();
}

function site_shortcode() {
   ob_start();
   site();
   $contentnav = ob_get_contents();
   ob_end_clean();
   return $contentnav;
}
add_shortcode('site-url', 'site_shortcode');

function slider() {

    $labels = array(
        'name'                     => _x('Slider', 'post type general name'),
        'singular_name'         => _x('Slider', 'post type singular name'),
        'add_new'                 => _x('Add New', 'slider'),
        'add_new_item'             => __('Add New slider'),
        'edit_item'             => __('Edit slider'),
        'new_item'                 => __('New slider'),
        'view_item'             => __('View slider'),
        'search_items'             => __('Search slider'),
        'not_found'              => __('Nothing found'),
        'not_found_in_trash'     => __('Nothing found in Trash'),
        'parent_item_colon'      => ''
    );

    $args = array(
        'labels'                 => $labels,
        'public'                 => true,
        'publicly_queryable'     => true,
        'show_ui'                 => true,
        'query_var'             => true,
        'menu_icon'             => 'dashicons-image-flip-horizontal',
        'rewrite'                 => true,
        'capability_type'         => 'post',
        'hierarchical'             => false,
        'menu_position'         => null,
        'supports'                 => array('title','editor','thumbnail')
      );

    register_post_type( 'slider' , $args );
}
add_action('init', 'slider');



function reviews_cpt() {

    $labels = array(
        'name'               => _x('Reviews', 'post type general name'),
        'singular_name'      => _x('Review', 'post type singular name'),
        'add_new'            => _x('Add New', 'Review'),
        'add_new_item'       => __('Add New Review'),
        'edit_item'          => __('Edit Review'),
        'new_item'           => __('New Review'),
        'view_item'          => __('View Review'),
        'search_items'       => __('Search Reviews'),
        'not_found'          => __('Nothing found'),
        'not_found_in_trash' => __('Nothing found in Trash'),
        'parent_item_colon'  => ''
    );

    $args = array(
        'labels'             => $labels,
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'query_var'          => true,
        'menu_icon'          => 'dashicons-image-flip-horizontal',
        'rewrite'            => true,
        'capability_type'    => 'post',
        'hierarchical'       => false,
        'menu_position'      => null,
        'supports'           => array('title','editor','thumbnail')
      );

    register_post_type( 'reviews' , $args );
}
add_action('init', 'reviews_cpt');



function faq() {

    $labels = array(
        'name'               => _x('FAQs', 'post type general name'),
        'singular_name'      => _x('FAQ', 'post type singular name'),
        'add_new'            => _x('Add New', 'FAQ'),
        'add_new_item'       => __('Add New FAQ'),
        'edit_item'          => __('Edit FAQ'),
        'new_item'           => __('New FAQ'),
        'view_item'          => __('View FAQ'),
        'search_items'       => __('Search FAQs'),
        'not_found'          => __('Nothing found'),
        'not_found_in_trash' => __('Nothing found in Trash'),
        'parent_item_colon'  => ''
    );

    $args = array(
        'labels'             => $labels,
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'query_var'          => true,
        'menu_icon'          => 'dashicons-image-flip-horizontal',
        'rewrite'            => true,
        'capability_type'    => 'post',
        'hierarchical'       => false,
        'menu_position'      => null,
        'supports'           => array('title','editor','thumbnail')
      );

    register_post_type( 'faq' , $args );
}
add_action('init', 'faq');



function designers() {

    $labels = array(
        'name'               => _x('Designers', 'post type general name'),
        'singular_name'      => _x('Designer', 'post type singular name'),
        'add_new'            => _x('Add Designer', 'Designer'),
        'add_new_item'       => __('Add New Designer'),
        'edit_item'          => __('Edit Designer'),
        'new_item'           => __('New Designer'),
        'view_item'          => __('View Designer'),
        'search_items'       => __('Search Designers'),
        'not_found'          => __('Nothing found'),
        'not_found_in_trash' => __('Nothing found in Trash'),
        'parent_item_colon'  => ''
    );

    $args = array(
        'labels'             => $labels,
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'query_var'          => true,
        'menu_icon'          => 'dashicons-image-flip-horizontal',
        'rewrite'            => true,
        'capability_type'    => 'post',
        'hierarchical'       => false,
        'menu_position'      => null,
        'supports'           => array('title','editor','thumbnail')
      );

    register_post_type( 'designers' , $args );
}
add_action('init', 'designers');



function awards_cpt() {

    $labels = array(
        'name'               => _x('Awards', 'post type general name'),
        'singular_name'      => _x('Award', 'post type singular name'),
        'add_new'            => _x('Add Award', 'Award'),
        'add_new_item'       => __('Add New Award'),
        'edit_item'          => __('Edit Award'),
        'new_item'           => __('New Award'),
        'view_item'          => __('View Award'),
        'search_items'       => __('Search Awards'),
        'not_found'          => __('Nothing found'),
        'not_found_in_trash' => __('Nothing found in Trash'),
        'parent_item_colon'  => ''
    );

    $args = array(
        'labels'             => $labels,
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'query_var'          => true,
        'menu_icon'          => 'dashicons-image-flip-horizontal',
        'rewrite'            => true,
        'capability_type'    => 'post',
        'hierarchical'       => false,
        'menu_position'      => null,
        'supports'           => array('title','editor','thumbnail')
      );

    register_post_type( 'awards' , $args );
}
add_action('init', 'awards_cpt');