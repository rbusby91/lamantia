<?php
/*
Template Name: Home Template
*/
?>

<section id="slider">
  <?php $loop = new WP_Query(array('post_type' => 'slide-carousel', 'posts_per_page' => 9)); ?>
  <?php include "templates/slider.php" ?>
  <?php wp_reset_query(); ?>
</section>

<?php get_template_part('templates/page', 'header'); ?>
<?php get_template_part('templates/content', 'page'); ?>