<?php get_template_part('templates/head'); ?>
<body <?php body_class(); ?>>

  <!--[if lt IE 9]>
    <div class="alert alert-warning">
      <?php _e('You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.', 'roots'); ?>
    </div>
  <![endif]-->

  <?php
    do_action('get_header');
    // Use Bootstrap's navbar if enabled in config.php
    if (current_theme_supports('bootstrap-top-navbar')) {
      get_template_part('templates/header-top-navbar');
    } else {
      get_template_part('templates/header');
    }
  ?>

  
<div class="container">
    <div class="row">
      <div class="col-sm-8">
        <div id="carousel" class="carousel slide" data-ride="carousel">
          <div class="carousel-inner" role="listbox">
            <?php
              $args = array( 'post_type' => 'slider' );
              $loop = new WP_Query( $args );
              while ( $loop->have_posts() ) : $loop->the_post(); ?>
                <div class="item">
                  <?php echo the_post_thumbnail('slider-img'); ?>
                  <div class="overlay">
                    <h2><?php the_title(); ?></h2>
                    <?php the_content(); ?>
                  </div>
                </div>
              <?php endwhile;
              wp_reset_postdata();
            ?>
          </div>
          <a class="left carousel-control" href="#carousel" role="button" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
          </a>
          <a class="right carousel-control" href="#carousel" role="button" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
          </a>
        </div>
      </div>
      <div class="col-sm-4">
        <?php echo do_shortcode('[contact-form-7 id="8" title="Consultation"]'); ?>
      </div>
    </div>
</div>


  <div class="wrap container" role="document">
    <div class="content row">
      <main class="main <?php echo roots_main_class(); ?>" role="main">
        <?php include roots_template_path(); ?>
      </main><!-- /.main -->
      <?php if (roots_display_sidebar()) : ?>
        <aside class="sidebar <?php echo roots_sidebar_class(); ?>" role="complementary">
          <?php include roots_sidebar_path(); ?>
        </aside><!-- /.sidebar -->
      <?php endif; ?>
    </div><!-- /.content -->
  </div><!-- /.wrap -->

  <?php get_template_part('templates/footer'); ?>

</body>
</html>
